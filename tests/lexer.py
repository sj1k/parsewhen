import pytest
import parsewhen

from parsewhen import Language


tests = [
    ('10pm 10:20pm 8.3 seconds 24:21 4min 10 days ago', [
        '10', 'pm', ' ', '10:20', 'pm', ' ', '8.3', ' ', 'seconds', ' ', '24:21',
        ' ', '4', 'min', ' ', '10', ' ', 'days', ' ', 'ago',
    ]),
    ('1st 3rd 21st 22nd', [
       '1', 'st', ' ', '3', 'rd', ' ', '21', 'st', ' ', '22', 'nd',
    ]),
    ('Monday Friday Sun', [
        'Monday', ' ', 'Friday', ' ', 'Sun',
    ]),
    ('10 hrs 5s and some text', [
        '10', ' ', 'hrs', ' ', '5', 's', ' ', 'and', ' ', 'some', ' ', 'text',
    ]),
]


@pytest.mark.parametrize('text, tokens', tests)
def test_tokens(text, tokens):
    lexed = [val for k, val in list(parsewhen.lexer.Lexer(text)) if k != Language.EOF]
    assert tokens == lexed
