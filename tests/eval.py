import pytest
import parsewhen

from datetime import datetime, timedelta


tests = [
    ('8 hours, 10 minutes, 5 seconds', timedelta(hours=8, minutes=10, seconds=5)),
    ('10am Next Friday', datetime(2020, 1, 10, 10)),
    ('1:31pm Next Friday', datetime(2020, 1, 10, 13, 31)),
    ('Last Friday', datetime(2019, 12, 27)),
    ('5am Sunday', datetime(2020, 1, 5, 5)),
    ('In 10 hours', datetime(2020, 1, 1, 10, 0)),
    ('next month 17th', datetime(2020, 2, 17)),
    ('1 fortnight', timedelta(14)),
]


def dates():
    return datetime(year=2020, month=1, day=1)


@pytest.mark.parametrize('text, result', tests)
def test_eval(text, result):
    assert parsewhen.parse(text, dates=dates) == result
