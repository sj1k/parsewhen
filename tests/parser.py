import pytest
import parsewhen


tests = [
    ('10 hours, 51 seconds. 24 hours.', [
        [['10', 'hours'], ['51', 'seconds']],
        ['. '],
        [['24', 'hours']],
        ['.'],
    ]),
    ('Monday, 10:31pm. November the 4th for 5 hours. Next Friday.', [
        ['Monday', ['10:31', 'pm'], 'November', ['4', 'th']],
        [' ', 'for', ' '],
        [['5', 'hours']],
        ['. '],
        [['Next', 'Friday']],
        ['.'],
    ]),
    ('6:51am 7 seconds. In 10 hours. 5 hours ago. Next Week 10pm. More words!', [
        [['6:51', 'am']],
        [' '],
        [['7', 'seconds']],
        ['. '],
        ['In', ['10', 'hours']],
        ['. '],
        [['5', 'hours'], 'ago'],
        ['. '],
        [['Next', 'Week'], ['10', 'pm']],
        ['. ', 'More', ' ', 'words', '!'],
    ]),
    ('10am 4pm 7am', [
        [['10', 'am']],
        [' '],
        [['4', 'pm']],
        [' '],
        [['7', 'am']],
    ]),
    ('8 hours 6 hours 3 hours just random text!', [
        [['8', 'hours']],
        [' '],
        [['6', 'hours']],
        [' '],
        [['3', 'hours']],
        [' ', 'just', ' ', 'random', ' ', 'text', '!'],
    ]),
    ('This is some text. 10pm', [
        ['This', ' ', 'is', ' ', 'some', ' ', 'text', '. '], [['10', 'pm']]
    ]),
    ('In something other text. Nonsense.', [
        ['In'], [' ', 'something', ' ', 'other', ' ', 'text', '. ', 'Nonsense', '.'],
    ])
]


@pytest.mark.parametrize('text, tree', tests)
def test_tree(text, tree):
    lex = parsewhen.lexer.Lexer(text)
    stripped = parsewhen.parser.tree_values(parsewhen.parser.Parser(list(lex)))
    stripped = list(stripped)
    assert stripped == tree
